import asyncio
import logging
from aiogram import Bot, Dispatcher, types
from aiogram.filters.command import Command
from random import*
a=0
b=''
# Включаем логирование, чтобы не пропустить важные сообщения
logging.basicConfig(level=logging.INFO)
# Объект бота
bot = Bot(token="6438994755:AAEDh3gMwTBtKxlhRKiFBRBAjVF6CJqq4H8")
# Диспетчер
dp = Dispatcher()

# Хэндлер на команду /start
@dp.message(Command("start"))
async def cmd_start(message: types.Message):
    await message.answer("Hello!")

# Запуск процесса поллинга новых апдейтов
async def main():
    await dp.start_polling(bot)

# Хэндлер на команду /test1
@dp.message(Command("RandomNumber100"))
async def cmd_test1(message: types.Message):
    a=randint(1,100)

    await message.reply(str(a))

# Хэндлер на команду /test2
async def cmd_test2(message: types.Message):
    await message.reply("Test 2")

dp.message.register(cmd_test2, Command("test2"))

if __name__ == "__main__":
    asyncio.run(main())