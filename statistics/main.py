from fastapi import FastAPI

app = FastAPI(
    title="SignalMan"
)

@app.get('/')
def message():
    return {'message': 'Hello World'}

@app.get('/get_data', tags=['get_data'])
def get_data():
    return {'message': 'result'}


